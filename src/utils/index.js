export const resolveAddressLines = string => {
  var addressArray = string.split(",");
  return {
    addressLine1: addressArray[0].trim(),
    addressLine2: addressArray[1].trim(),
    addressLine3: addressArray[2].trim(),
    town: addressArray[5].trim(),
    county: addressArray[6].trim()
  };
};

const formatAddressLine = string => {
  return string.replace(/, ,+/g, ",");
};

export const resolveAddressDropdownItemText = string => {
  var addressArray = string.split(",");
  var addressString = `${addressArray[0].trim()}, ${addressArray[1].trim()}, ${addressArray[5].trim()}`;
  return formatAddressLine(addressString);
};

export const resolveAddressListItem = ai => {
  var addressString = `${ai.addressLine1}, ${ai.addressLine2}, ${ai.addressLine3}, ${ai.town}, ${ai.county}.`;
  return {
    address: formatAddressLine(addressString),
    time: `Time at address: ${ai.stayYear} ${ai.stayMonth}`
  };
};
