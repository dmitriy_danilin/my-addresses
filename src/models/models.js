import { ApiUrl, ApiKey, YearLabel, MonthsLabel } from "../constants";

export const addressForm = {
  state: {
    stayYear: "",
    stayMonth: "",
    addressToSearch: "",
    addressLine1: "",
    addressLine2: "",
    addressLine3: "",
    town: "",
    county: "",
    addressIsSelected: false
  },

  reducers: {
    setStayYear(state, yearValue) {
      return {
        stayYear: yearValue === YearLabel ? "" : yearValue,
        stayMonth: state.stayMonth,
        addressToSearch: state.addressToSearch,
        addressLine1: state.addressLine1,
        addressLine2: state.addressLine2,
        addressLine3: state.addressLine3,
        town: state.town,
        county: state.county,
        addressIsSelected: state.addressIsSelected
      };
    },

    setStayMonth(state, monthValue) {
      return {
        stayYear: state.stayYear,
        stayMonth: monthValue === MonthsLabel ? "" : monthValue,
        addressToSearch: state.addressToSearch,
        addressLine1: state.addressLine1,
        addressLine2: state.addressLine2,
        addressLine3: state.addressLine3,
        town: state.town,
        county: state.county,
        addressIsSelected: state.addressIsSelected
      };
    },

    setAddressToSearch(state, address) {
      return {
        stayYear: state.stayYear,
        stayMonth: state.stayMonth,
        addressToSearch: address,
        addressLine1: state.addressLine1,
        addressLine2: state.addressLine2,
        addressLine3: state.addressLine3,
        town: state.town,
        county: state.county,
        addressIsSelected: state.addressIsSelected
      };
    },

    setAddressLine1(state, addressLine1) {
      return {
        stayYear: state.stayYear,
        stayMonth: state.stayMonth,
        addressToSearch: state.addressToSearch,
        addressLine1: addressLine1,
        addressLine2: state.addressLine2,
        addressLine3: state.addressLine3,
        town: state.town,
        county: state.county,
        addressIsSelected: state.addressIsSelected
      };
    },

    setAddressLine2(state, addressLine2) {
      return {
        stayYear: state.stayYear,
        stayMonth: state.stayMonth,
        addressToSearch: state.addressToSearch,
        addressLine1: state.addressLine1,
        addressLine2: addressLine2,
        addressLine3: state.addressLine3,
        town: state.town,
        county: state.county,
        addressIsSelected: state.addressIsSelected
      };
    },

    setAddressLine3(state, addressLine3) {
      return {
        stayYear: state.stayYear,
        stayMonth: state.stayMonth,
        addressToSearch: state.addressToSearch,
        addressLine1: state.addressLine1,
        addressLine2: state.addressLine2,
        addressLine3: addressLine3,
        town: state.town,
        county: state.county,
        addressIsSelected: state.addressIsSelected
      };
    },

    setTown(state, town) {
      return {
        stayYear: state.stayYear,
        stayMonth: state.stayMonth,
        addressToSearch: state.addressToSearch,
        addressLine1: state.addressLine1,
        addressLine2: state.addressLine2,
        addressLine3: state.addressLine3,
        town: town,
        county: state.county,
        addressIsSelected: state.addressIsSelected
      };
    },

    setCounty(state, county) {
      return {
        stayYear: state.stayYear,
        stayMonth: state.stayMonth,
        addressToSearch: state.addressToSearch,
        addressLine1: state.addressLine1,
        addressLine2: state.addressLine2,
        addressLine3: state.addressLine3,
        town: state.town,
        county: county,
        addressIsSelected: state.addressIsSelected
      };
    },

    setAddressLines(state, addressLines) {
      return {
        stayYear: state.stayYear,
        stayMonth: state.stayMonth,
        addressToSearch: state.addressToSearch,
        addressLine1: addressLines.addressLine1,
        addressLine2: addressLines.addressLine2,
        addressLine3: addressLines.addressLine3,
        town: addressLines.town,
        county: addressLines.county,
        addressIsSelected: true
      };
    },

    resetAddressForm(state) {
      return {
        stayYear: "",
        stayMonth: "",
        addressToSearch: "",
        addressLine1: "",
        addressLine2: "",
        addressLine3: "",
        town: "",
        county: "",
        addressIsSelected: false
      };
    }
  }
};

export const addressList = {
  state: [],

  reducers: {
    addAddress(state, address) {
      return [...state, address];
    }
  }
};

export const addressSearchBar = {
  state: {
    addresses: [],
    isNotFound: false,
    isLoading: false,
    isFailed: false
  },

  reducers: {
    setIsLoading(state) {
      return {
        addresses: state.addresses,
        isNotFound: false,
        isLoading: true,
        isFailed: false
      };
    },

    setIsFailed(state) {
      return {
        addresses: state.addresses,
        isNotFound: false,
        isLoading: false,
        isFailed: true
      };
    },

    setIsCompleted(state, data) {
      return {
        addresses: data,
        isNotFound: false,
        isLoading: false,
        isFailed: false
      };
    },

    setIsNotFound(state) {
      return {
        addresses: [],
        isNotFound: true,
        isLoading: false,
        isFailed: false
      };
    },

    resetAddressSearchBar(state) {
      return {
        addresses: [],
        isNotFound: false,
        isLoading: false,
        isFailed: false
      };
    }
  },

  effects: dispatch => ({
    async fetchAddressesAsync(addressToSearch, rootState) {
      try {
        dispatch.addressSearchBar.setIsLoading();
        let response = await fetch(
          `${ApiUrl}/${addressToSearch}?api-key=${ApiKey}`
        );
        let json = await response.json();

        if (response.status === 200) {
          json.addresses.length === 0
            ? dispatch.addressSearchBar.setIsNotFound()
            : dispatch.addressSearchBar.setIsCompleted(json.addresses);
        } else {
          dispatch.addressSearchBar.setIsNotFound();
        }
      } catch (e) {
        dispatch.addressSearchBar.setIsFailed();
      }
    }
  })
};
