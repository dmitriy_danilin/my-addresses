export const ApiUrl = "https://api.getAddress.io/find";
export const ApiKey = "9vJ3rLPhZU6qb1uOaowcGQ14886";

export const YearLabel = "Year";
export const MonthsLabel = "Months";

export const StayLengthValues = [
  {
    label: YearLabel,
    numbers: [1, 2, 3, 4, 5, 6]
  },
  {
    label: MonthsLabel,
    numbers: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
  }
];
