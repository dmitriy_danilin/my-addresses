import React, { Component } from "react";
import { connect } from "react-redux";

import AddressListItem from "../AddressListItem";
import { resolveAddressListItem } from "../../utils";

import "./styles.scss";

class AddressList extends Component {
  render() {
    return (
      <div className="address-list-area">
        {this.props.adresses.map(addressItem => (
          <AddressListItem address={resolveAddressListItem(addressItem)} />
        ))}
      </div>
    );
  }
}

const mapState = state => ({
  adresses: state.addressList
});

export default connect(mapState)(AddressList);
