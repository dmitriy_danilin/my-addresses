import React, { Component } from "react";
import { connect } from "react-redux";

import { resolveAddressLines } from "../../utils";
import AddressesDropdown from "../../controls/AddressDropdown";

import "./styles.scss";

class AddressDropdown extends Component {
  optionChangeHandler = event => {
    let currentAddress = this.props.addresses[event];
    let addressLines = resolveAddressLines(currentAddress);
    this.props.setAddressLines(addressLines);
  };

  render() {
    return (
      <div className="address-picker-area">
        <label className="app-label">Select your address:</label>
        <AddressesDropdown
          placeholderText="Select your address"
          values={this.props.addresses}
          onChange={this.optionChangeHandler}
        />
      </div>
    );
  }
}

const mapState = state => state.addressSearchBar;

const mapDispatch = ({ addressForm: { setAddressLines } }) => ({
  setAddressLines: addressLines => setAddressLines(addressLines)
});

export default connect(
  mapState,
  mapDispatch
)(AddressDropdown);
