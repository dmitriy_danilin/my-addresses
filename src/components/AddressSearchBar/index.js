import React, { Component } from "react";
import { connect } from "react-redux";
import { InputGroup, InputGroupAddon, Button, Input } from "reactstrap";

import "./styles.scss";

class AddressSearchBar extends Component {
  textChangeHandler = event =>
    this.props.setAddressToSearch(event.target.value);
  searchClickHandler = () =>
    this.props.fetchAddressesAsync(this.props.addressToSearch);

  render() {
    return (
      <div className="search-area">
        <InputGroup>
          <Input
            onChange={this.textChangeHandler}
            value={this.props.addressToSearch}
          />
          <InputGroupAddon addonType="append">
            <Button color="secondary" onClick={this.searchClickHandler}>
              Search
            </Button>
          </InputGroupAddon>
        </InputGroup>
        {this.props.addressNotFound && (
          <label className="not-found-label">Not found</label>
        )}
      </div>
    );
  }
}

const mapState = state => ({
  addressToSearch: state.addressForm.addressToSearch,
  addressNotFound: state.addressSearchBar.isNotFound
});

const mapDispatch = ({
  addressForm: { setAddressToSearch },
  addressSearchBar: { fetchAddressesAsync }
}) => ({
  setAddressToSearch: value => setAddressToSearch(value),
  fetchAddressesAsync: addressToSearch => fetchAddressesAsync(addressToSearch)
});

export default connect(
  mapState,
  mapDispatch
)(AddressSearchBar);
