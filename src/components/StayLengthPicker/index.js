import React, { Component } from "react";
import { connect } from "react-redux";

import { StayLengthValues, YearLabel, MonthsLabel } from "../../constants";
import StayLengthDropdown from "../../controls/StayLengthDropdown";

import "./styles.scss";

class StayLengthPicker extends Component {
  yearChangeHandler = year => this.props.setStayYear(year);
  monthChangeHandler = month => this.props.setStayMonth(month);

  renderDropdown(value) {
    return (
      <StayLengthDropdown
        placeholderText={value.label === YearLabel ? YearLabel : MonthsLabel}
        label={value.label === YearLabel ? YearLabel : MonthsLabel}
        values={value.numbers}
        onChange={
          value.label === YearLabel
            ? this.yearChangeHandler
            : this.monthChangeHandler
        }
      />
    );
  }

  render() {
    return (
      <div className="period-area">
        <div className="period-selector">
          <span className="app-label">
            How long did you stay at your
            <span className="app-label period-selector__label--bold">
              {" "}current address
            </span>
            ?
          </span>
          {StayLengthValues.map(value => this.renderDropdown(value))}
        </div>
      </div>
    );
  }
}

const mapState = state => state.addressForm;

const mapDispatch = ({ addressForm: { setStayYear, setStayMonth } }) => ({
  setStayYear: value => setStayYear(value),
  setStayMonth: value => setStayMonth(value)
});

export default connect(
  mapState,
  mapDispatch
)(StayLengthPicker);
