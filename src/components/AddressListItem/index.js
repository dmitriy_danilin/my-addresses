import React from "react";

import "./styles.scss";

const AddressListItem = props => {
  return (
    <div className="address-item-area">
      <div>
        <label className="app-label info-area__label">
          {props.address.address}
        </label>
      </div>
      <div>
        <label className="app-label info-area__label">
          {props.address.time}
        </label>
      </div>
    </div>
  );
};

export default AddressListItem;
