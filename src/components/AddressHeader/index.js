import React from 'react';

import './styles.scss';

const AddressHeader = props => {
  return (
    <div className="info-area">
      <label className="app-label info-area__title">Home address</label>
      <label className="app-label">Please enter your home address for the last 3 years.</label>
    </div>
  );
}

export default AddressHeader;