import React, { Component } from "react";
import { connect } from "react-redux";

import ChevronAnimation from "./ChevronAnimation";
import AddressLineInput from "./AddressLineInput";

import "./styles.scss";

class AddressLines extends Component {
  render() {
    return (
      <div className="address-area">
        <ChevronAnimation />
        <AddressLineInput
          label="Address line 1:"
          value={this.props.addressLine1}
          onChange={this.props.setAddressLine1}
        />
        <AddressLineInput
          label="Address line 2:"
          value={this.props.addressLine2}
          onChange={this.props.setAddressLine2}
        />
        <AddressLineInput
          label="Address line 3:"
          value={this.props.addressLine3}
          onChange={this.props.setAddressLine3}
        />
        <AddressLineInput
          label="Town:"
          value={this.props.town}
          onChange={this.props.setTown}
        />
        <AddressLineInput
          label="County:"
          value={this.props.county}
          onChange={this.props.setCounty}
        />
      </div>
    );
  }
}

const mapState = state => state.addressForm;

const mapDispatch = ({
  addressForm: {
    setAddressLine1,
    setAddressLine2,
    setAddressLine3,
    setTown,
    setCounty
  }
}) => ({
  setAddressLine1: addressLine1 => setAddressLine1(addressLine1),
  setAddressLine2: addressLine2 => setAddressLine2(addressLine2),
  setAddressLine3: addressLine3 => setAddressLine3(addressLine3),
  setTown: town => setTown(town),
  setCounty: county => setCounty(county)
});

export default connect(
  mapState,
  mapDispatch
)(AddressLines);
