import React from 'react';
import { Input } from 'reactstrap';

import './styles.scss';

const AddressLineInput = props => {
  return (
    <div>
      <label className="app-label">{props.label}</label>
      <Input className="custom-text-box" value={props.value} onChange={e => props.onChange(e.target.value, e)} />
    </div>
  );
}

export default AddressLineInput;