import React from 'react';

import './styles.scss';

const ChevronAnimation = props => {
  return (
    <div className="chevron-group">
      <div className="chevron-group__chevron"></div>
      <div className="chevron-group__chevron"></div>
    </div>
  );
}

export default ChevronAnimation;