import React, { Component } from "react";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";

export default class AddressesDropdown extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dropdownOpen: false,
      selectedText: this.props.placeholderText,
      selectedIndex: -1
    };

    this.handleSelect = this.handleSelect.bind(this);
  }

  resolveSelectedIndex = selectedText => {
    let index = -1;
    this.props.values.forEach((element, i) => {
      if (element.indexOf(selectedText) !== -1) {
        index = i;
        return;
      }
    });
    return index;
  };

  toggle = () =>
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));

  handleSelect = e => {
    let index = this.resolveSelectedIndex(e.target.innerText);
    this.setState({ selectedText: e.target.innerText, selectedIndex: index });
    this.props.onChange(index);
  };

  renderDropdonItem(value, i) {
    return (
      <DropdownItem key={i} onClick={this.handleSelect}>
        {value}
      </DropdownItem>
    );
  }

  render() {
    return (
      <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle caret>{this.state.selectedText}</DropdownToggle>
        <DropdownMenu>
          {this.props.values.map((value, i) =>
            this.renderDropdonItem(value, i)
          )}
        </DropdownMenu>
      </Dropdown>
    );
  }
}
