import React, { Component } from "react";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";

export default class StayLengthDropdown extends Component {
  state = {
    dropdownOpen: false,
    selectedText: this.props.placeholderText
  };

  toggle = () =>
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));

  handleSelect = e => {
    this.setState({ selectedText: e.target.innerText });
    this.props.onChange(e.target.innerText);
  };

  renderDropdonItem(value) {
    return (
      <DropdownItem onClick={this.handleSelect}>
        {value} {this.props.label}
      </DropdownItem>
    );
  }

  render() {
    return (
      <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle caret>{this.state.selectedText}</DropdownToggle>
        <DropdownMenu>
          <DropdownItem onClick={this.handleSelect}>None</DropdownItem>
          {this.props.values.map(value => this.renderDropdonItem(value))}
        </DropdownMenu>
      </Dropdown>
    );
  }
}
