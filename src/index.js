import React from 'react';
import ReactDOM from 'react-dom';
import { init } from "@rematch/core";
import { Provider } from "react-redux";

import App from './containers/App';
import * as models from './models/models'

import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const store = init({
  models,
});

const Root = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

ReactDOM.render(<Root />, document.getElementById('root'));