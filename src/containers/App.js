import React, { Component } from "react";
import { connect } from "react-redux";

import AddressHeader from "../components/AddressHeader";
import StayLengthPicker from "../components/StayLengthPicker";
import AddressSearchBar from "../components/AddressSearchBar";
import AddressList from "../components/AddressList";
import AddressDropdown from "../components/AddressDropdown";
import AddressLines from "../components/AddressLines";

import "./index.scss";

const _ = require("lodash/core");

class App extends Component {
  validateData = () => {
    return (
      (this.props.stayYear !== "" ||
        this.props.stayMonth !== "" ||
        this.props.stayYear !== "None" ||
        this.props.stayMonth !== "None") &&
      this.props.addressLine1 !== "" &&
      this.props.town !== "" &&
      this.props.county !== ""
    );
  };

  confirmClickHandler = () => {
    if (!this.validateData()) {
      alert("Mandatory fields are not filled");
      return;
    }

    this.props.addAddress(
      _.pick(this.props, [
        "addressLine1",
        "addressLine2",
        "addressLine3",
        "town",
        "county",
        "stayYear",
        "stayMonth"
      ])
    );

    this.props.resetAddressForm();
    this.props.resetAddressSearchBar();
  };

  render() {
    return (
      <div>
        <AddressHeader />
        <AddressList />
        <StayLengthPicker />
        <AddressSearchBar />
        {this.props.addresses.length > 0 && <AddressDropdown />}
        {this.props.addressIsSelected && <AddressLines />}
        <div className="action-button" onClick={this.confirmClickHandler}>
          Confirm and continue
        </div>
      </div>
    );
  }
}

const mapState = state => ({
  stayYear: state.addressForm.stayYear,
  stayMonth: state.addressForm.stayMonth,
  addressIsSelected: state.addressForm.addressIsSelected,
  addressLine1: state.addressForm.addressLine1,
  addressLine2: state.addressForm.addressLine2,
  addressLine3: state.addressForm.addressLine3,
  town: state.addressForm.town,
  county: state.addressForm.county,
  addresses: state.addressSearchBar.addresses
});

const mapDispatch = ({
  addressList: { addAddress },
  addressForm: { resetAddressForm },
  addressSearchBar: { resetAddressSearchBar }
}) => ({
  addAddress: value => addAddress(value),
  resetAddressForm: () => resetAddressForm(),
  resetAddressSearchBar: () => resetAddressSearchBar()
});

export default connect(
  mapState,
  mapDispatch
)(App);
